var dcTemp = {
  data: null,
  tabId: null,
  attachmentId: null
};

/**
 * Finds and reads a deletion conditions attachment file.
 * 
 * @returns the deletion conditions data object and the
 *          corresponding `ComposerAttachment`'s ID if
 *          a valid attachment has been found, or the
 *          default DC data or null otherwise.
 */
async function readDcFile() {
  // Find an attachment with the right name
  var attachment = await findAttachment("__dctimedata__", dcTemp.tabId);
  // Fill default data
  var dc = {...dcBaseData};
  var attachmentId = null;
  // Valid attachment found: try loading it
  if (attachment != null) {
    attachmentId = attachment.id;
    // Read the attachment's text
    var f = await getFile(attachment);
    var text = await f.text();
    // Must have two conditions separated by a semicolon
    var parts = text.trim().split(';');
    if (parts.length == 2) {
      // Absolute condition unchecked if starting by an underscore
      dc.absChecked = !(parts[0].charAt(0) == '_');
      // Remove the underscore if needed to read the condition
      if (!dc.absChecked) parts[0] = parts[0].substring(1);
      // Same with relative condition
      dc.relChecked = !(parts[1].charAt(0) == '_');
      if (!dc.relChecked) parts[1] = parts[1].substring(1);
      // Try converting the absolute date string to a Date object
      dc.abs = isoToDate(parts[0]);
      // Try splitting the relative conditions,
      // set it in the data object if valid
      var rel = splitRel(parts[1]);
      if (rel != null) {
        dc.rel = rel;
      }
    }
  }
  return {
    attachmentId: attachmentId,
    data: dc
  };
}

/**
 * Creates a DOM `File` object containing
 * formatted deletion conditions.
 * 
 * @param {dcBaseData} data The conditions data object.
 * @returns the `File` with formatted conditions inside it.
 */
function createDcFile(data) {
  // Convert the absolute date to a string (empty if no date)
  var abs = (data.abs) ? data.abs.toISOString() : "";
  // Prepend an underscore if absolute condition unchecked
  var absChecked = (data.absChecked) ? '' : '_';
  var r = data.rel;
  var rel = "";
  // Convert relative condition to string
  // if at least one value is not zero
  if (!Object.keys(r).every((k) => {return r[k] == 0})) {
    rel += (r.yr != 0 ? r.yr : "") + "|";
    rel += (r.mt != 0 ? r.mt : "") + "|";
    rel += (r.dy != 0 ? r.dy : "") + "|";
    rel += (r.hr != 0 ? r.hr : "") + "|";
    rel += (r.mn != 0 ? r.mn : "");
  }
  // Prepend an underscore if relative condition unchecked
  var relChecked = (data.relChecked) ? '' : '_';
  var txt = `${absChecked}${abs};${relChecked}${rel}`;
  return textFile("__dctimedata__", txt);
}

/**
 * Creates or updates a deletion condition attachment.
 * 
 * @param {dcBaseData} data The deletion conditions data object.
 * @param {number} tabId The composer tab's ID.
 * @param {number} attachmentId The ID of the `ComposerAttachment` object to 
 *                              update. If omitted, creates a new attachment.
 * @returns the new deletion condition `ComposerAttachment`'s ID.
 */
async function writeDcFile(data, tabId, attachmentId = null) {
  var f = createDcFile(data);
  var attachment
  if (attachmentId == null) {
    attachment = await addAttachment(f, tabId)
  } else {
    attachment = await updateAttachment(f, tabId, attachmentId);
  }
  return attachment.id;
}

/**
 * Updates the absolute condition in the attachment file.
 */
async function updateAbs() {
  var abs = document.getElementById("abs");
  var absdt = document.getElementById("absDt");
  var abstm = document.getElementById("absTm");
  dcTemp.data.absChecked = abs.checked;
  if (absdt.value || abstm.value) {
    // Use the input's date or the current date if empty
    var date = new Date(absdt.value || Date.now());
    // Use the input's time or midnight if empty
    var time = (abstm.value || "00:00").split(":");
    date.setHours(time[0], time[1], 0);
    dcTemp.data.abs = date;
  }
  dcTemp.attachmentId = await writeDcFile(
    dcTemp.data, dcTemp.tabId, dcTemp.attachmentId
  );
}

/**
 * Updates the relative condition in the attachment file.
 * 
 * @param {string} id The input tag's ID. If omitted, 
 *                    only the checked state is updated.
 */
async function updateRel(id = null) {
  var rel = document.getElementById("rel");
  dcTemp.data.relChecked = rel.checked;
  if (id) {
    // Update corresponding delay (yr/mt/dy/hr/mn)
    var value = document.getElementById(id).value || "0";
    dcTemp.data.rel[id] = parseInt(value);
  }
  dcTemp.attachmentId = await writeDcFile(
    dcTemp.data, dcTemp.tabId, dcTemp.attachmentId
  );
}

/**
 * Restores the deletion conditions when re-opening the pop-up.
 * 
 * @param {dcBaseData} data The conditions data object.
 */
function restoreInput(data) {
  // Restore absolute condition checkbox
  if (data.absChecked) {
    var abs = document.getElementById("abs");
    abs.checked = true;
  }

  // Restore absolute condition if found
  if (data.abs) {
    // We can't use absdt.valueAsDate even if it would
    // be easier because it converts the date to GMT
    // and we want to use what is actually stored, so
    // we manually convert the individual date values
    // and set them as a string.
    // Restore date
    var absdt = document.getElementById("absDt");
    var yr = data.abs.getFullYear();
    var mt = timeFormat(data.abs.getMonth() + 1);
    var dy = timeFormat(data.abs.getDate());
    absdt.value = `${yr}-${mt}-${dy}`;
    // Restore time
    var abstm = document.getElementById("absTm");
    var hr = timeFormat(data.abs.getHours());
    var mn = timeFormat(data.abs.getMinutes());
    abstm.value = `${hr}:${mn}`;
  }

  // Restore relative condition checkbox
  if (data.relChecked) {
    var rel = document.getElementById("rel");
    rel.checked = true;
  }

  // Restore relative condition if not zero
  var yr = document.getElementById("yr");
  var mt = document.getElementById("mt");
  var dy = document.getElementById("dy");
  var hr = document.getElementById("hr");
  var mn = document.getElementById("mn");
  if (data.rel.yr != 0) yr.value = data.rel.yr;
  if (data.rel.mt != 0) mt.value = data.rel.mt;
  if (data.rel.dy != 0) dy.value = data.rel.dy;
  if (data.rel.hr != 0) hr.value = data.rel.hr;
  if (data.rel.mn != 0) mn.value = data.rel.mn;
}

// Update absolute condition on input/checkbox value change
for (var input of document.getElementsByClassName("absdc")) {
  input.addEventListener("input", () => {
    updateAbs();
  });
}
// Update relative condition value on input value change
for (var input of document.getElementsByClassName("reldcval")) {
  input.addEventListener("input", (e) => {
    updateRel(e.currentTarget.id);
  });
}
// Update relative condition state on checkbox value change
document.getElementById("rel").addEventListener("input", () => {
  updateRel();
});


// Get the current composer window and
// restore the deletion conditions
browser.tabs.query({
  active: true,
  currentWindow: true,
}).then((tabs) => {
  dcTemp.tabId = tabs[0].id;
  // Read DC attachment and restore conditions
  readDcFile().then((res) => {
    dcTemp.attachmentId = res.attachmentId;
    dcTemp.data = res.data;
    restoreInput(res.data);
  });
});