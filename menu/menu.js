const acc = document.getElementById("account");
const trash = document.getElementById("recycle");
const freq = document.getElementById("freq");

/**
 * Fills the account selector with the user's accounts.
 */
async function addAccounts() {
    var accounts = await browser.accounts.list();
    for (var account of accounts) {
        var accopt = document.createElement("option");
        accopt.text = account.name;
        accopt.value = account.id;
        acc.appendChild(accopt);
    }
}

/**
 * Fills the trash folder selector with the account's folders.
 * 
 * @param {number} accountId The account's ID.
 */
async function addFolders(accountId) {
    // Get the account's data
    var account = await browser.accounts.get(accountId);
    // Remove all previous options
    while (trash.children.length > 0) {
        trash.children[0].remove();
    }
    // Add a default option
    var noFolder = document.createElement("option");
    noFolder.text = "None";
    noFolder.value = "null";
    trash.appendChild(noFolder);
    // Add the account's folders recursively
    for (var folder of account.folders) {
        var nodes = getFoldersRec(folder, "");
        for (var node of nodes) {
            trash.appendChild(node);
        }
    }
}

/**
 * Creates a list of DOM option tags from a root folder.
 * 
 * @param {MailFolder} folder The root folder.
 * @param {string} tree The hierarchy level indicator.
 * @returns an array of option tags with each folder's
 *          name as text and its path as value.
 */
function getFoldersRec(folder, tree) {
    var folders = [];
    var option = document.createElement("option");
    option.text = tree + folder.name;
    option.value = folder.path;
    folders.push(option);
    //if there are subFolders in the folder, the length is bigger than 0
    if (folder.subFolders.length > 0) {
        for (var f of folder.subFolders) {
            folders.push(...getFoldersRec(f, tree + "> "));
        }
    }
    return folders;
}

// Updates the trash folder selector when
// changing the selected account.
acc.addEventListener("input", async () => {
    await addFolders(acc.value);
    trash.value = getTrashFolder(acc.value);
});

// Updates the trash folder settings for the
// currently selected account.
trash.addEventListener("input", () => {
    var foldersStr = localStorage.getItem("trashFolders");
    var folders = JSON.parse(foldersStr);
    folders[acc.value] = trash.value;
    localStorage.setItem("trashFolders", JSON.stringify(folders));
});

// Updates the deletion check frequency.
freq.addEventListener("input", () => {
    var time = freq.valueAsNumber;
    // Set minimum/default value if needed
    if (isNaN(time) || time < 1)
        time = 1;
    // Save config
    localStorage.setItem("deletionFrequency", time);
    browser.runtime.sendMessage({
        type: "frequencyChanged",
        freq: time
    });
});

// Requests a manual check to the background script's worker.
document.getElementById("check").addEventListener("click", () => {
    // Request deletion check
    browser.runtime.sendMessage({
        type: "checkExpired"
    });
});

// Initializes the popup's content
addAccounts().then(async () => {
    var initialAcc = acc.value;
    await addFolders(initialAcc);
    freq.value = localStorage.getItem("deletionFrequency") || 1;
    trash.value = getTrashFolder(initialAcc);
});
