/**
 * Deletes emails with a deletion time older than the current time.
 */
async function deleteExpired() {
  // Original conditions database state
  var db = getDcMails();
  var now = new Date();

  // Find expired emails. db.abs is ordered on deletion dates:
  // only the first element of the list has to be checked
  while (db.abs.length > 0 && new Date(db.abs[0].time) <= now) {
    // Get the first email in the list
    var abs = db.abs.shift();
    var mails = await browser.messages.query({headerMessageId: abs.id});
    // Delete all corresponding emails
    for (var mail of mails.messages) {
      var account = mail.folder.accountId;
      // Trash folder associated with the email's account
      var folder = getTrashFolder(account);
      if (folder != "null") {
        // Delete the email
        await browser.messages.move([mail.id], {
          accountId: account,
          path: folder
        });
      }
    }
  }
  // Update database
  localStorage.setItem("dcmails", JSON.stringify(db));
}

/**
 * Returns the deletion conditions database or creates
 * an empty one if it doesn't exist.
 * 
 * @returns the conditions database as a JSON object.
 */
function getDcMails() {
  var src = localStorage.getItem("dcmails");
  if (src == null) {
    return {
      abs: {},
      read: {}
    };
  } else {
    return JSON.parse(src);
  }
}

/**
 * Compares two dates as ISO strings for array ordering.
 * 
 * @param {string} a The first date.
 * @param {string} b The second date.
 * @returns 1 if a > b, -1 if a < b, else 0.
 */
function absComparator(a, b) {
  var adt = isoToDate(a.time);
  var bdt = isoToDate(b.time)
  if (adt < bdt) {
    return -1;
  } else if (adt > bdt) {
    return 1;
  }
  return 0;
}

/**
 * Adds a deletion condition on an absolute date
 * to the database for a given email.
 * 
 * @param {string} mailId The email's message-id header.
 * @param {string} abs The deletion date as an ISO string.
 * @returns the updated conditions database.
 */
function addAbsDc(mailId, abs) {
  var db = getDcMails();
  // Check the condition's validity
  if (isoToDate(abs)) {
    // Find an existing condition for the given ID
    var existing = db.abs.findIndex(d => d.id == mailId);
    if (existing != -1) {
      // Email already has an absolute
      // condition: update it
      db.abs[existing].time = abs;
    } else {
      // No condition for this email:
      // add a new one
      db.abs.push({
        id: mailId,
        time: abs
      });
    }
    // Sort the conditions
    db.abs.sort(absComparator);
    localStorage.setItem("dcmails", JSON.stringify(db));
  }
  return db;
}

/**
 * Adds a deletion condition on a delay after reading
 * a given email to the database.
 * 
 * @param {string} mailId The email's message-id header.
 * @param {string} rel The deletion delay formatted as 
 *                     "years|months|days|hours|minutes".
 * @returns the updated conditions database.
 */
function addRelDc(mailId, rel) {
  var db = getDcMails();
  if (isRelValid(rel)) {
    db.read[mailId] = rel;
    localStorage.setItem("dcmails", JSON.stringify(db));
  }
  return db;
}

// Adds newly received emails to the conditions database
browser.messages.onNewMailReceived.addListener(async (folder, list) => {
  for (mail of list.messages) {
    browser.messages.listAttachments(mail.id).then(async (files) => {
      // If there are attachments in the email, check them
      if (files.length > 0) {
        var i = 0;
        var found = false;
        // Try finding a correct DC file
        while (i < files.length && !found) {
          // Must be called __dctimedata__ and be in plain text
          if (files[i].contentType == "text/plain" && files[i].name == "__dctimedata__") {
            var file = await browser.messages.getAttachmentFile(mail.id, files[i].partName);
            var text = await file.text();
            // __dctimedata__ contains an absolute deletion date and a
            // delay after reading separated by ";": split them
            var parts = text.trim().split(';');
            // If there aren't two parts or both of them
            // are empty strings: not a valid DC
            if (parts.length == 2 && !parts.every(s => s == "")) {
              var abs = isoToDate(parts[0]);
              // The following functions won't change the
              // database if the given conditions are wrong
              // Try adding the absolute condition
              addAbsDc(mail.headerMessageId, parts[0]);
              // Try adding the on-read condition
              addRelDc(mail.headerMessageId, parts[1]);
              // If both conditions are valid or one of them
              // is empty, we found a valid DC file
              found = (parts[0] == "" || abs != null) &&
                      (isRelEmpty(parts[1]) || isRelValid(parts[1]));
            }
          }
          i++;
        }
      }
    });
  }
});

// Checks for read emails
browser.messages.onUpdated.addListener((msg, changedProp) => {
  if (changedProp.read) {
    var db = getDcMails();
    // Check if the email has an on-read condition
    var rel = db.read[msg.headerMessageId];
    if (rel) {
      // Check and split the condition
      var split = splitRel(rel);
      if (split != null) {
        // Convert the relative time to a delay in minutes
        var delay = split.yr * 525600 +
                    split.mt * 43800 +
                    split.dy * 1440 +
                    split.hr * 60 +
                    split.mn;
        // Add the delay (converted to milliseconds)
        // to the current time
        var date = new Date(Date.now() + delay*60000);
        var absStr = db.abs.find(v => v.id == msg.headerMessageId);
        if (absStr) {
          // If the same email also has an absolute condition,
          // use the earliest date as expiration date
          var abs = new Date(absStr);
          if (abs < date) {
            date = abs;
          }
        }
        // Add or update the absolute condition
        db = addAbsDc(msg.headerMessageId, date.toISOString());
        // Remove the on-read condition
        delete db.read[msg.headerMessageId];
        localStorage.setItem("dcmails", JSON.stringify(db));
      }
    }
  }
});

// Cleans deletion conditions attachment before sending an email
browser.compose.onBeforeSend.addListener(async (tab, details) => {
  var attachment = await findAttachment("__dctimedata__", tab.id);
  if (attachment != null) {
    // Conditions attachment found in the current email:
    // extract the content for checking
    var f = null;
    var text = await (await getFile(attachment)).text();
    var parts = text.trim().split(';');
    if (parts.length == 2) {
      // Remove the relative condition if unchecked
      if (parts[1].charAt(0) == "_")
        parts[1] = "";
      // Cleaned file content
      var newtext = "";
      // Add the absolute condition
      // if checked and valid
      var abs = isoToDate(parts[0]);
      if (abs != null) {
        newtext += parts[0];
      }
      newtext += ";";
      // Add the relative condition
      // if checked and valid
      if (isRelValid(parts[1])) {
        newtext += parts[1];
      }
      // Create a new attachment if the
      // cleaned content isn't empty
      if (newtext != ";") {
        f = textFile("__dctimedata__", newtext);
      }
    }
    if (f != null) {
      // Update attachment if correctly cleaned
      await updateAttachment(f, tab.id, attachment.id);
    } else {
      // Error cleaning the attachment: remove it
      await browser.compose.removeAttachment(tab.id, attachment.id);
    }
  }
});

/**
 * Fills the extension's storage with default values
 * if needed and starts the deletion worker.
 * 
 * @returns the initial worker.
 */
async function initialize() {
  var maildb = localStorage.getItem("dcmails");
  if (maildb == null) {
    // No database: create an empty one
    localStorage.setItem("dcmails", '{"abs": [], "read": {}}');
  } else {
    // Database exists: check and clean it
    var jsondb = JSON.parse(maildb);
    var updateddb = {};
    // Add absolute conditions or create
    // empty object if not found
    updateddb.abs = jsondb.abs || {};
    // Add on read conditions or create
    // empty object if not found
    updateddb.read = jsondb.read || {};
    localStorage.setItem("dcmails", JSON.stringify(updateddb));
  }
  // Get account settings
  var trashFolders = localStorage.getItem("trashFolders");
  var folders = JSON.parse(trashFolders) || {};
  var accounts = await browser.accounts.list();
  for (var account of accounts) {
    // Set default trash folder if account
    // doesn't have one
    if (!folders[account.id]) {
      folders[account.id] = "null";
    }
  }
  localStorage.setItem("trashFolders", JSON.stringify(folders));
  var freqText = localStorage.getItem("deletionFrequency")
  var freq = parseInt(freqText);
  if (!freq || freq < 1) {
    // Set default frequency if not found / too low
    freq = 1;
    localStorage.setItem("deletionFrequency", `${freq}`);
  }
  // Initial check
  deleteExpired();
  // Start deletion worker
  return createDeletionWorker(freq);
}

/**
 * Creates a background worker that regularly
 * checks for expired emails.
 * 
 * @param {number} frequency The time in minutes between each check.
 * @returns the worker's ID.
 */
function createDeletionWorker(frequency) {
  return setInterval(() => {
    deleteExpired();
  }, frequency * 60000);
}

// Initialize the extension
initialize().then((w) => {
  var worker = w;

  // Receive messages from composer/browser scripts
  browser.runtime.onMessage.addListener((msg, sender, response) => {
    switch (msg.type) {
      case "checkExpired":
        // Manually check for expired emails
        deleteExpired();
        break;
      case "frequencyChanged":
        // Update the worker's frequency
        clearInterval(worker);
        worker = createDeletionWorker(msg.freq);
        break;
    }
  });
});