/**
 * Default deletion condition attachment content.
 */
const dcBaseData = {
    abs: null,
    absChecked: false,
    rel: {
        yr: 0, mt: 0, dy: 0, hr: 0, mn: 0
    },
    relChecked: false
};
  
/**
 * Backward-compatible wrapper to get a File object
 * from a ComposeAttachment object.
 * Thunderbird deprecated `ComposeAttachment.getFile()` 
 * in favor of `browser.compose.getAttachmentFile()`:
 * use this function provided by Thunderbird developers
 *  as a replacement that can work on any version.
 * 
 * @param {ComposerAttachment} attachment The attachment object.
 * @returns the DOM `File` object contained in the attachment.
 */
function getFile(attachment) {
    let file = "getAttachmentFile" in browser.compose
    ? messenger.compose.getAttachmentFile(attachment.id)
    : attachment.getFile();
    return file;
}

/**
 * Finds an attachment by its name in an email being composed.
 * 
 * @param {string} filename The attachment file's name.
 * @param {number} tabId The composer tab's ID.
 * @returns the corresponding `ComposerAttachment` or
 *          null if not found.
 */
async function findAttachment(filename, tabId) {
    var files = await browser.compose.listAttachments(tabId);
    var i = 0;
    var attachment = null;
    while (i < files.length && attachment == null) {
        if (files[i].name == filename) {
            attachment = files[i]
        } else {
            i++;
        }
    }
    return attachment;
}

/**
 * Adds an attachment from a DOM `File` object
 * to an email being composed.
 * 
 * @param {File} file The `File` object.
 * @param {number} tabId The composer tab's ID.
 * @returns the `ComposerAttachment` object.
 */
async function addAttachment(file, tabId) {
    var attachment = await browser.compose.addAttachment(
        tabId, {file: file}
    );
    return attachment;
}

/**
 * Updates an existing attachment in an email being composed,
 * replacing its content with that of a DOM `File` object.
 * 
 * @param {File} file The `File` object.
 * @param {number} tabId The composer tab's ID.
 * @param {number} attachmentId The original `ComposeAttachment`'s ID
 * @returns the updated `ComposerAttachment` object.
 */
async function updateAttachment(file, tabId, attachmentId) {
    var attachment = await browser.compose.updateAttachment(
        tabId, attachmentId, {file: file}
      );
    return attachment;
}

/**
 * Creates a DOM `File` object from a string.
 * 
 * @param {string} name The file's name.
 * @param {string} text The file's content.
 * @returns the `File` object.
 */
function textFile(name, text) {
    return new File([text], name, {
        type: "text/plain",
    });
}

/**
 * Gets the trash folder choosen by the user for an account.
 * 
 * @param {number} accountId The account's ID.
 * @returns the trash folder's path.
 */
function getTrashFolder(accountId) {
    var foldersStr = localStorage.getItem("trashFolders");
    var folders = JSON.parse(foldersStr);
    return folders[accountId];
}

/**
 * Converts an ISO date string to a `Date` object.
 * 
 * @param {*} str The ISO string.
 * @returns a `Date` object corresponding to the given
 *          date string or null if the string was invalid.
 */
function isoToDate(str) {
    if (str == "") return null;
    var date = new Date(str);
    if (date.toString() == "Invalid Date") {
        return null
    }
    return date;
}

/**
 * Checks if an on-read condition string is empty.
 * 
 * @param {string} rel The condition string.
 * @returns true if the condition is empty, false otherwise.
 */
function isRelEmpty(rel) {
    return rel == "||||" || rel == "";
}

/**
 * Checks if an on-read condition string is right.
 * 
 * @param {string} rel The condition string.
 * @returns true if the condition is valid, false otherwise.
 */
function isRelValid(rel) {
    var split = rel.split("|");
    return  !isRelEmpty(rel) &&
            split.length == 5 &&
            split.every(v => !isNaN(v));
}

/**
 * Splits an on-read condition string if valid.
 * 
 * @param {string} str The condition string.
 * @returns a JSON object representing the on-read
 *          condition's values or null if the string is invalid.
 */
function splitRel(str) {
    if (isRelValid(str)) {
        var rel = str.split("|");
        var relObj = {};
        relObj.yr = parseInt(rel[0]) || 0;
        relObj.mt = parseInt(rel[1]) || 0;
        relObj.dy = parseInt(rel[2]) || 0;
        relObj.hr = parseInt(rel[3]) || 0;
        relObj.mn = parseInt(rel[4]) || 0;
        return relObj;
    }
    return null;
}

/**
 * Formats an integer number to a 2-digits string.
 * 
 * @param {number} num The number to format.
 * @returns the number as a string with a leading zero if needed.
 */
function timeFormat(num) {
    return num.toLocaleString(
        undefined, {minimumIntegerDigits: 2}
    );
}